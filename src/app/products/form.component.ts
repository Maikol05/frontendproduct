import { Component, OnInit } from '@angular/core';
import { Producto } from './producto';
import { ProductoService } from './producto.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private producto: Producto = new Producto()
  private titulo: String = "Crear Producto"

  constructor(private productService: ProductoService, 
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this.cargarProducto();
  }

  cargarProducto(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.productService.getProduct(id).subscribe((producto) => this.producto = producto)
      }
    })
  }

  public create(): void{
    this.productService.create(this.producto).subscribe(producto => {
      this.router.navigate(['/products'])
      Swal.fire(
        'producto creado con exito',
        'producto registrado',
        'success'
      )
  });
  }

  public update():void{
    this.productService.update(this.producto).subscribe(producto =>{
      this.router.navigate(["/products"])
      Swal.fire(
        'producto actualizado con exito',
        'producto registrado',
        'success'
      )
    })
  }
}
